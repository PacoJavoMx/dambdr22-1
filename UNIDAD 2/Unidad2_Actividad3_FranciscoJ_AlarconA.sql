USE inscripciones_f_alarcon;
##1--Agrega la relación entre la tabla profesor y la tabla grupo
ALTER TABLE profesor
ADD FOREIGN KEY (id) REFERENCES grupo(id);
##2--Agrega el atributo CUPO con tipo de dato entero en la tabla grupo
ALTER TABLE grupo
ADD cupo int;
##3--Establece como valor por default el caracter L en el atributo GRADO de la tabla profesor
ALTER TABLE profesor
MODIFY grado char DEFAULT 'L' ;
##4--Inserta 3 registros a cada una de las tablas de la base de datos, los registros se deberan de insertar en cada una de las tablas con las ids 1,2,y,3
##TABLA Profesor
##TABLA Persona
INSERT INTO persona(id)
VALUES
('1'),
('2'),
('3');

##TABLA Alumno
INSERT INTO alumno(id)
VALUES
('1'),
('2'),
('3');
##TABLA INSCRIPCION
INSERT INTO inscripcion(id)
VALUES
('1'),
('2'),
('3');
##TABLA GRUPO
INSERT INTO grupo(id)
VALUES
('1'),
('2'),
('3');
##TABLA ASIGNATURA
INSERT INTO asignatura(id)
VALUES
('1'),
('2'),
('3');
##TABLA PERIODO

INSERT INTO periodo(id)
VALUES
('1'),
('2'),
('3');
##TABLA SALON
INSERT INTO salon(id)
VALUES
('1'),
('2'),
('3');
##TABLA DIA
INSERT INTO dia(id)
VALUES
('1'),
('2'),
('3');
##TABLA HORA
INSERT INTO hora(id)
VALUES
('1'),
('2'),
('3');
##TABLA horario
INSERT INTO horario(id)
VALUES
('1'),
('2'),
('3');
##TABLA profesor
INSERT INTO profesor(id)
VALUES
('1'),
('2'),
('3');

#5--ACTUALIZAR EL NOMBRE DE LA ASIGNATURA A DESARROLLO DE APLICACIONES CON RDBMS DONDE EL ID SEA IGUAL 3, ADMINISTRACION Y CONTROL DE PROYECTOS DONDE EL ID SEA IGUAL A 1 Y PROGRAMACION ORIENTADA A OBJETOS DONDE EL ID SEA IGUAL A 2
UPDATE `asignatura` SET `nombre` = 'DESARROLLO DE APLICACIONES CON RDBMS' WHERE `asignatura`.`id` = 3;
UPDATE `asignatura` SET `NOMBRE` = 'ADMINISTRACION Y CONTROL DE PROYECTOS' WHERE `asignatura`.`id`=2;
UPDATE `asignatura` SET `NOMBRE` = 'PROGRAMACION ORIENTADA A OBJETOS' WHERE `asignatura`.`id`=1;
##6--AUMENTA 3 LUGARES A TODOS LOS GRUPOS
INSERT INTO grupo (id)
VALUES
('4'),
('5'),
('6');
#7--ELIMINA EL GRUPO CON EL ID 2 
DELETE FROM `grupo` WHERE `grupo`.`id` = 2;

#8--INSERTA 2 REGISTROS MÁS A LA TABLA INSCRIPCION Y PROFESOR QUE CORRESPONDAN A LOS ID's 4 y 5
INSERT INTO profesor(id)
VALUES
('4'),
('5');
INSERT INTO inscripcion(id)
VALUES
('4'),
('5');
##9--CAMBIA EL GRUPO A CERO DEL GRUPO CON EL ID 1
UPDATE `grupo` SET `id` = '0' WHERE `grupo`.`id` = 1;
#10-- ACTUALIZA LA FECHA DE INSCRIPCION CON LA SIGUIENTE INFORMACION:
## EL ID 1 DEBERA TENER LA FECHA 1 DE SEPTIEMBRE DEL 2021
UPDATE `inscripcion` SET `fecha` = '2021-09-01' WHERE `inscripcion`.`id` = 1;
## EL ID 3 DEBERA TENER LA FECHA 15 DE SEPTIEMBRE DEL 2021
UPDATE `inscripcion` SET `fecha` = '2021-09-15' WHERE `inscripcion`.`id` = 3;
## EL ID 5 DEBERA TENER LA FECHA 17 DE SEPTIEMBRE DEL 2021
UPDATE `inscripcion` SET `fecha` = '2021-09-17' WHERE `inscripcion`.`id` = 5;