#CREACION DE BASES DE DATOS inscripciones_f_alarcon

CREATE DATABASE inscripciones_f_alarcon;

USE inscripciones_f_alarcon;

#CREACION TABLA PERSONA
CREATE TABLE persona(
id int PRIMARY KEY,
nombre varchar(75),
apaterno varchar(75),
amaterno varchar(75)
);
#CREACION TABLA PROFESOR
CREATE TABLE profesor(

id int PRIMARY KEY,
persona_id int,
num_trabajador varchar(10),
cedula varchar(10),
grado char(1)
);
#CREACION TABLA ALUMNO
CREATE TABLE alumno(
id int PRIMARY KEY,
persona_id int,
num_cuenta char(9),
generacion int
);
#CREACION TABLA INSCRIPCION
CREATE TABLE inscripcion(
id int PRIMARY KEY,
alumno_id int,
grupo_id int,
hora time,
fecha date, 
host char(15)
);
#CREACION TABLA GRUPO
CREATE TABLE grupo(
id  int PRIMARY KEY,
asignatura_id int,
periodo_id int,
clave varchar(4)
);
#CREACION TABLA ASIGNATURA
CREATE TABLE asignatura (
id int PRIMARY KEY,
nombre varchar(100),
clave char(4)
);
#CREACION TABLA PERIODO
CREATE TABLE periodo(
id int PRIMARY KEY,
clave char(4),
fec_inicio date,
fec_fin date
);
#CREACION TABLA HORARIO
CREATE TABLE horario(
id int PRIMARY KEY,
hora_id int,
grupo_id int,
dia_id int,
salon_id int
);
#CREACION TABLA SALON
CREATE TABLE salon(
id int PRIMARY KEY,
clave char(2),
edificio_piso char(1)
);
#CREACION TABLA DIA
CREATE TABLE dia(
id int PRIMARY KEY,
nombre varchar(6)
);
#CREACION TABLA HORA
CREATE TABLE hora(
id int PRIMARY KEY,
hora_ini time,
hora_fin time
);
#MODIFICAMOS LAS TABLAS AÑADIENDO SUS RESPECTIVAS LLAVES FORANEAS 
ALTER TABLE inscripcion
ADD FOREIGN KEY (alumno_id) REFERENCES alumno(id);

ALTER TABLE alumno
ADD FOREIGN KEY (persona_id) REFERENCES persona(id);

ALTER TABLE profesor
ADD FOREIGN KEY (persona_id) REFERENCES persona(id);

ALTER TABLE inscripcion
ADD FOREIGN KEY (grupo_id) REFERENCES grupo(id);

ALTER TABLE grupo
ADD FOREIGN KEY (asignatura_id) REFERENCES asignatura(id);

ALTER TABLE grupo
ADD FOREIGN KEY (periodo_id) REFERENCES periodo(id);

ALTER TABLE horario
ADD FOREIGN KEY (grupo_ID) REFERENCES grupo(id);

ALTER TABLE horario
ADD FOREIGN KEY(salon_id) REFERENCES salon(id);

ALTER TABLE horario
ADD FOREIGN KEY (dia_id) REFERENCES dia(id);

ALTER TABLE horario
ADD FOREIGN KEY (hora_id) REFERENCES hora(id);